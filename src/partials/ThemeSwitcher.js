import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon, faSun } from "@fortawesome/free-solid-svg-icons";
import { THEMES } from "../utils/consts";

const ICONS = {
  [THEMES.LIGHT]: faMoon,
  [THEMES.DARK]: faSun,
};

export default class ThemeSwitcher extends React.Component {
  render() {
    const { onClick, theme } = this.props;

    return (
      <button
        className="app__dark-mode-btn icon level-right"
        onClick={() => {
          if (onClick) onClick();
        }}
      >
        <FontAwesomeIcon icon={ICONS[theme]} color={"#4D5B6B"} />
      </button>
    );
  }
}
