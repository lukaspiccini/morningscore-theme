const THEMES = {
  LIGHT: "LIGHT",
  DARK: "DARK",
};

export { THEMES };
