const persistTheme = (theme) => {
  localStorage.setItem("theme", theme);
};

const getTheme = () => localStorage.getItem("theme");

export { persistTheme, getTheme };
